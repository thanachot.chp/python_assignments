# คำนวณการหารและเก็บจำนวนครั้งที่หารลงตัว

divided = 0 # ตัวหาร
divisor = 0 # ตัวถูกหาร
count = 0 # จำนวนครั้งที่หารลงตัว

# รับค่าตัวหาร
print("Input your nmuber for multiples (between1-10)?")
divided = int(input())

# ถ้าตัวเลขของตัวหารไม่อยู่ระหว่าง 0-10 จะไม่เข้า Loop
if (divided >=1 and divided <=10) :
    # รับค่าตัวถูกหาร
    print("Input your nmuber for check (-1 to stop):")
    divisor = int(input())

    # loop สำหรับหารลงตัว ใส่ -1 จะจบ loop
    while (divisor != -1): 
        # หาลงตัวค่าและเก็บจำนวนครั้งที่หารลงตัว
        if ((divisor % divided) == 0) :
            print("%d is evenly divisible by %d " % (divisor,divided))
            count += 1
        # หารไม่ลงตัว
        else :
            print("%d is not evenly divisible by %d " % (divisor,divided))
        print("Input your nmuber for check (-1 to stop):")
        divisor = int(input())

    # แสดงจำนวนครั้งที่หารลงตัวและจำนวนที่ใช้ในการหาร
    print("ํYou number of time that is divisible {0}.".format(count))

# ตัวหารมีตัวเลขไม่อยู่ระหว่าง 1-10
else :
    print("Please enter the nmuber for multiples")
