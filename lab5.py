# รับจำนวนนักเรียนและคะแรรเพื่อหาคะแนนที่มากและน้อยที่สุด

students = 0 # จำนวนนักเรียน
name = 0 # ชื่อนักเรียน
nameAll = [] # เก็บชื่อนักเรียนทั้งหมด
exam = 0 # คะแนน
examAll = [] # เก็บคะแนนทั้งหมด
i = 0 # ทำให้เงื่อนไขการทำซ้ำ
maxScore = 0 # คะแนนที่เยอะที่สุด
minScore = 100 # คะแนนน้อยที่สุด
indexMax = 0 # 
indexMin = 0 #

# รับจำนวนนักเรียน
print("How many racers in the F1 (maximum is 20)?")
students = int(input())

# นักเรียนไม่เกิน 20
if (students<=20):
    # รับค่าคะแนนของนักเรียนทุกคน
    for i in range (0,students,1):
        print("Racer name {0} ?".format(i+1))
        name = str(input())
        nameAll.append(name)
        print("score for Racer {0} ?".format(i+1))
        exam = int(input())
        examAll.append(exam)
        # หาคะแนนที่เยอะที่สุด
        if (examAll[i]>maxScore) :
            maxScore=examAll[i]
            indexMax=i
        # หาคะแนนที่น้อยที่สุด
        if (examAll[i]<=minScore) :
            minScore=examAll[i]
            indexMin=i
    
    # แสดงคะแนนที่มากและน้อยที่สุด
    print("{0} had the highest score in the competiton {1}".format(nameAll[indexMax],maxScore))
    print("{0} had the lowest score in the competiton {1}".format(nameAll[indexMin],minScore))

# นักเรียนเกิน 20 คน
else :
    print("There are too many students")
