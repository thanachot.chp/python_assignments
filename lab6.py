# รับค่ารหัสผ่านและรับจำนวนเงินรายปีเพื่อหาภาษี

# ฟังชันค์ตรวจสอบตัวอักษรทั้งหมดคือตัวเลข
def checkallDigts (stringToCheck) :
    
    length = len(stringToCheck) #ความยาวรหัส
    chkDigit = 0 # เก็บค่าของฟังชันค์ isdigit
    
    #
    for i in range (0,length,1):
        chkDigit = stringToCheck[i].isdigit()
        if (chkDigit == False) :
            break
    
    return chkDigit

# ตรวจสอบความยาวของรหัส
def getTaxID () :
    code = [5] # รหัส
    codelen = 0 # ความยาวรหัส
    result = 0 # เก็บค่าของฟังชันค์ strlen
    chkdigit = 0 # เก็บค่าของฟังชันค์ checkallDigits
    
    # ตรวจสอบตัวอักษร
    while (chkdigit == 0  or codelen!=4) :
        print("Enter your tax ID (4 digits, 0000 to end): ")
        code = input()
        codelen = len(code)
        if (code == "0000") :
            result = 0
        else :
            result = 1
        chkdigit = checkallDigts(code)
        # ความยาวตัวเลขไม่ถึง 4
        if (codelen<4) :
            print("Invalid tax ID, be 4 characters long")
        # ความยาวตัวเลขเกิน 4
        elif (codelen>4) : 
            print("Invalid tax ID, be 4 characters long")
        # ตัวอักษรไม่ใช่ตัวเลขทั้งหมด
        if (chkdigit == False ) :
            print("Invalid tax ID, must be all digits")
    return result

# เก็บค่าเงินและตรวจสอบตัวอักษร
def getIncome () :
    
    income = [20] # จำนวนเงิน
    chkdigit = 0 # เก็บค่าของฟังชันค์ checkallDigits
    
    # เก็บค่าเงินและตรวจสอบว่าเป็นตัวเลขทั้งหมด
    print("what is your yearly income in bath? ")
    income = (input()) 
    chkdigit = checkallDigts(income)
    # ค่าเงินไม่เป็นตัวเลขทั้งหมด
    while (chkdigit == False) :
        print("Invalid income, must be all digits ")
        print("what is your yearly income in bath? ")
        income = (input()) 
        chkdigit = checkallDigts(income)
    income = int(income)

    return income

income  = 0 # เก็บค่าของฟังชันค์  getIncome 
vat = 0 # ภาษี
totalVat = 0 #  รวมภาษี
value = 1 # เก็บค่าของฟังชันค์ getTaxID
category = [0,0,0,0]# รับค่าจำนวนครั้งในการคำนวนภาษีแต่ละcategory 

while (1) :
    
    # เรียกใช้ฟังชันค์รับและตรวจสอบรหัส
    value = getTaxID() 
    # รหัสเป็น 0000
    if (value==0) :
        print("total tax revenue collected {0}".format(totalVat))
        for i in range (0,4,1):
            print("Tax catrgory 1 count {0}".format(category[i]))
        break
    # รหัสไม่เป็น 0000
    else :
        # เรียกใช้ฟังชันค์รับและตรวจสอบค่าเงิน
        income = getIncome() 
        # ค่าเงิน<=50000
        if (income <= 50000) :
            vat = income * 0.0 
            print("\tyour tax bill is {0} baht".format(vat))
            category[0] += 1
        
        # ค่าเงิน<=200000
        elif (income <= 200000) :
            vat = income * 0.1
            print("\tyour tax bill is {0} baht".format(vat))
            totalVat = totalVat + vat 
            category[1] += 1

        # ค่าเงิน<=1000000
        elif (income <= 1000000) : 
            vat = income * 0.2
            print("\tyour tax bill is {0} baht".format(vat))
            totalVat = totalVat + vat 
            category[2] += 1

        # ค่าเงิน<1000000
        else :
            vat = income  * 0.3
            print("\tyour tax bill is {0} baht".format(vat))
            totalVat = totalVat + vat 
            category[3] += 1














