# รับจำนวนสินค้าและคำนวณภาษีในรูปแบบใบเสร็จ

numProduct = 0 # จำนวนสินค้า
item = 0 #ราคาสินค้า
itemAll = [] #จำนวนราคาสินค้าของสินค้าแต่ละชิ้น
vat = 0 # จำนวนภาษี
vatAll = [] #จำนวนภาษีทั้งหมดของสินค้าแต่ละชิ้น
total = 0 #รวมราคาและภาษีของทุกต่อชิ้น
totalAll = [] #จำนวนรวมราคาและภาษีของสินค้าต่อชิ้น
totalFinal = 0 #รวมราคาและภาษีของทุกสินค้า
priceFinal = 0 #รวมราคาสินค้า
vatFinal = 0 #ราคาภาษีทั้งหมด

# รับจำนวนสินค้า
print("How many item in your basket?")
numProduct = int(input())

# สินค้าเกิน 20 ชิ้นหรือไม่
if (numProduct<=20):
    # รับราคาสินค้า
    for i in range (0,numProduct,1):
        print("price for item %d" % (i+1))
        Item = float(input())
        itemAll.append(Item)
        # คำนวนvatและราคารวม
        if (Item>=100):
            vat = Item * 0.07
            vatAll.append(vat)
            total = (vat + Item)
            totalAll.append(total)
        else :
            vatAll.append(0)
            totalAll.append(Item)      
# แสดงใบเสร็จ
    print("Here is you receipt")
    print("Item        vat        Item Total")
    print("--------------------------------")
    for i in range (0,numProduct,1):
        print("{0:.3f}      {1:.3f}      {2:.3f}".format(itemAll[i],vatAll[i],totalAll[i]))
        priceFinal = priceFinal + itemAll[i]
        vatFinal = vatFinal + vatAll[i]
        totalFinal = totalFinal + totalAll[i]
    print("--------------------------------")
    print("{0:.3f}     {1:.3f}     {2:.3f}".format(priceFinal,vatFinal,totalFinal))
# สินค้าเกิน 20 ชิ้น
else : 
    print("the program cannot handle this many items")


